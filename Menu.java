package main;

import animation.BombermanAnimation;
import floor.Floor;

import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class Menu implements ActionListener {

    private static JFrame menu = new JFrame();
    private static JButton button = new JButton("Start game");
    private static ImageIcon bomber;
    private static JLabel label;
    private static final int TIME_STEP = 1;
    private static int width = 18;
    private static int height = 10;
    private static int nrOfEnemies = 5;
    private static Timer clockTimer = null;
    private static BombermanAnimation bombermanAnimation;
    private static GameOver gameOver;

    public Menu() {
        button.addActionListener(this);
        bomber = new ImageIcon(this.getClass().getResource("imagines/bomberman.jpg"));
        label = new JLabel(bomber);
        label.setSize(900, 500);
        menu.add(label, BorderLayout.CENTER);
        menu.add(button, BorderLayout.EAST);
        menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        menu.setTitle("Bomberman game");
        menu.setLocation(390, 150);
        menu.pack();
        menu.setVisible(true);

    }

    public static void startGame() {
        JFrame frame = new JFrame();
        Floor floor = new Floor(width, height, nrOfEnemies);
        bombermanAnimation = new BombermanAnimation(floor);
        floor.createPlayer(bombermanAnimation, floor);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        floor.addFloorListener(bombermanAnimation);
        frame.setLayout(new BorderLayout());
        frame.add(bombermanAnimation, BorderLayout.CENTER);
        frame.pack();
        frame.setLocation(390, 150);
        frame.setVisible(true);

        Action doOneStep = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                tick(frame, floor, menu);
            }
        };
        clockTimer = new Timer(TIME_STEP, doOneStep);
        clockTimer.setCoalesce(true);
        clockTimer.start();
    }

    private static void tick(JFrame frame, Floor floor, JFrame menu) {
        if (floor.getIsGameOver()) {
            gameOver = new GameOver(clockTimer, frame, floor);
        } else {
            floor.moveEnemies();
            floor.bombCountdown();
            floor.explosionHandler();
            floor.characterInExplosion();
            floor.notifyListeners();
        }
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        menu.dispose();
        startGame();
        File file = new File("audio/music.wav");
        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(file);
            Clip clip = AudioSystem.getClip();
            clip.open(audio);

            clip.start();
        } catch (UnsupportedAudioFileException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (LineUnavailableException ex) {
            ex.printStackTrace();
        }
    }
}
