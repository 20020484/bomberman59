package main;

import floor.Floor;

import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

public class GameOver {

    private static JFrame gameOver = new JFrame();
    private static ImageIcon bomber;
    private static JLabel label;
    private Timer clockTimer;
    private  JFrame frame;


    public Action pa = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            gameOver.dispose();
            Menu menu= new Menu();

        }
    };

    public GameOver(Timer clockTimer, JFrame frame, Floor floor) {
        this.clockTimer = clockTimer;
        this.frame = frame;
        bomber = new ImageIcon(this.getClass().getResource("imagines/gameover (1).png"));
        label = new JLabel(bomber);
        label.setSize(500, 200);
        gameOver.add(label, BorderLayout.CENTER);
        gameOver.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameOver.setLocation(350, 100);
        gameOver.pack();
        gameOver.setVisible(true);
        gameOver(clockTimer, frame, floor);
        label.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "playagain");
        label.getActionMap().put("playagain", pa);
    }

    private static void gameOver(Timer clockTimer, JFrame frame, Floor floor) {

        File file = new File("audio/gameOver.wav");
        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(file);
            Clip clip = AudioSystem.getClip();
            clip.open(audio);

            clip.start();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }

        clockTimer.stop();
        frame.dispose();

    }

}
